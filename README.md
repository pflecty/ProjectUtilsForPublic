# ProjectUtilsForPublic

#### 项目介绍
ProjectUtilsForPublic项目导航工具是一款辅助eclipse j2ee项目开发插件，本eclipse插件最要针对公司内容的项目开发，运行在eclipse中，主要功能通过选中的文件名导航到该文件并使用默认编辑器打开，大大简化繁琐了Ctrl+Shift+R搜索资源的步骤，给您愉快的编码工作！
如非本公司项目的小伙伴可以自行修改代码，已达到开发的需要，本插件不收取任何费用，请放心使用。


#### 软件架构
软件架构说明

开发环境
Eclipse4.4
Eclipse for RCP and RAP Developers
Version: Luna Service Release 2 (4.4.2)
Build id: 20150219-0600

JDK 1.7



#### 安装教程


一、复制插件到eclipse根路径的plugins文件夹里面

二、启动eclipse




#### 使用说明


功能、
1、定位到当前文件
 


2、定位到Views文件夹
 

3、定位到指定控制类
 

4、复制当前编辑的文件名
 
复制当前编辑的文件名，不复制后缀名
5、跳转指定jsp
 
 
鼠标选中要跳转的文件，点击 ，跳转



6、跳转指定mode
 

7、跳转指定Service
 

8、跳转到指定Controller
 

9、跳转到mybatis的Mapper映射文件
 
####项目结构：


│  plugin.xml 插件配置文件
├─data 一些可打包的数据
├─icons 按钮图标文件夹
│      controller.gif
│      copy-cur-edit-file-name.png
│      current-file.png
│      join_to_cur_controller.png
│      join_to_cur_service.png
│      join_to_cur_view.png
│      join_to_mode.png
│      mapper.png
│      views.png  
└─src 源码src
    ├─projectutils
    │  │  Activator.java
    │  │  
    │  ├─actions 动作组
    │  │      CopyCurEditFileName.java 复制当前编辑文件名称
    │  │      JoinToSelectController.java 跳转指定控制类
    │  │      JoinToSelectDao.java 跳转指定Dao类
    │  │      JoinToSelectMapper.java跳转到mybatis的Mapper映射文件
    │  │      JoinToSelectMode.java跳转指定mode
    │  │      JoinToSelectService.java跳转指定Service
    │  │      JoinToSelectView.java跳转到指定jsp
    │  │      PositionAdminViewsFolder.java 定位Admin视图文件夹
    │  │      PositionControllerAdminFolder.java 定位Admin控制类文件夹
    │  │      PositionControllerFolder.java 定位控制类文件夹
    │  │      PositionToCurFile.java 定位当前文件
    │  │      PositionViewsFolder.java 定位视图文件夹
    │  │      
    │  └─utils 工具类
    │          CommonUtil.java 常用工具类
    │          LogUtil.java 日志工具类
    │          PlugUtil.java 插件工具类
    │          PositionUtil.java 定位工具类
    │          
    └─test 测试
            Test.java 测试java
            





#### 参与贡献


感兴趣的小伙伴可以，在eclipse rcp中导入项目，进行修改，由于时间的关系，源码中并没有太多的注释，存在描述不清楚或其他bug等问题，请谅解。


